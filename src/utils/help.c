/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:19:57 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 16:03:12 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

char			**typestring(void)
{
	char	**result;

	if (!(result = (char**)malloc(sizeof(char*) * 4)))
		error("Memory is not allocated");
	result[0] = "Standard commands:";
	result[1] = "Message Digest commands:";
	result[2] = "Cipher commands:";
	result[3] = NULL;
	return (result);
}

void			printtype(t_sslmodule **module)
{
	char	**tstr;
	uint	count;
	uint	count2;

	count = 0;
	tstr = typestring();
	while (tstr[count] != NULL)
	{
		ft_putchar('\n');
		ft_putendl(tstr[count]);
		count2 = 0;
		while (module[count2] != NULL)
		{
			if (module[count2]->type == count)
				ft_putendl(module[count2]->name);
			count2++;
		}
		count++;
	}
	ft_putchar('\n');
	free(tstr);
}

void			printhelp(t_flag *flag, t_sslmodule *module)
{
	if (!ft_strncmp(flag->flag, "-h", ft_strlen(flag->flag)) &&
	checkflagoption(flag, 1))
		flagusage(module);
}
