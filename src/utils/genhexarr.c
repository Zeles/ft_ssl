/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   genhexarr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:29:07 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 17:29:07 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

char	*genhexarr(void)
{
	char	*arr;
	int		count;

	if (!(arr = (char*)malloc(sizeof(char) * 16)))
		error("Memory is not allocated");
	count = 0;
	while (count < 10)
	{
		arr[count] = '0' + count;
		count++;
	}
	while (count < 16)
	{
		arr[count] = 'a' + (count - 10);
		count++;
	}
	return (arr);
}
