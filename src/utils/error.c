/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:19:39 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 19:59:24 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	logerror(char *str)
{
	if (str != NULL)
	{
		ft_putstr("ft_ssl: ");
		ft_putendl(str);
	}
}

void	error(char *str)
{
	if (str != NULL)
	{
		ft_putstr("ft_ssl: ");
		ft_putendl(str);
	}
	exit(EXIT_FAILURE);
}
