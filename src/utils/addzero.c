/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   addzero.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:20:44 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 12:35:36 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

char			*addzero(char *str, size_t zerocount)
{
	char	*new;
	size_t	len;
	size_t	newlen;
	size_t	count;

	len = ft_strlen(str);
	newlen = len + (zerocount - len);
	new = ft_strnew(newlen);
	count = -1;
	while (++count < newlen)
		new[count] = '0';
	ft_strcpy(new + (newlen - len), str);
	return (new);
}
