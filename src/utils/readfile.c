/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:19:14 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 18:04:47 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

char	*readfile(const char *path, int *err)
{
	char	*str;
	int		fd;

	fd = open(path, O_RDONLY);
	if (fd <= 0)
	{
		*err |= 1;
		return (NULL);
	}
	str = readfd(fd, err);
	close(fd);
	return (str);
}

char	*readfd(int fd, int *err)
{
	int		r;
	char	buff[BUFF_SIZE + 1];
	char	*str;
	char	*del;

	ft_bzero(buff, BUFF_SIZE + 1);
	str = ft_strnew(0);
	while ((r = read(fd, buff, BUFF_SIZE)) > 0)
	{
		buff[r] = '\0';
		del = str;
		str = ft_strjoin(str, buff);
		ft_strdel(&del);
	}
	if (err != NULL && r == -1)
		*err |= 2;
	return (str);
}
