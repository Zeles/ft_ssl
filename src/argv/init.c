/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:20:07 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 17:20:07 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

t_argv			initargv(int argc, char **argv)
{
	t_argv	result;

	result.argc = argc;
	result.argv = argv;
	return (result);
}
