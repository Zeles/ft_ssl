/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 20:03:23 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 18:15:17 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

void			md5readstdin(t_sslmodule *module)
{
	t_flag		*sflag;
	t_flag		*pflag;
	char		*tmp;
	char		*out;

	sflag = findflag(module->flags, "-s");
	pflag = findflag(module->flags, "-p");
	if ((checkflagoption(sflag, 1) && !checkflagoption(pflag, 1))
	|| (module->filecount != 0 && !checkflagoption(pflag, 1)))
		return ;
	module->stdin = readfd(0, NULL);
	if (checkflagoption(pflag, 1))
	{
		tmp = md5toa(md5(module->stdin, ft_strlen(module->stdin)));
		out = ft_strjoin(module->stdin, tmp);
		ft_strdel(&tmp);
	}
	else
		out = md5toa(md5(module->stdin, ft_strlen(module->stdin)));
	ft_strdel(&(module->stdin));
	module->output = addoutput(module->output, out);
}

char			*md5genqr(int isfile, char *hashname, char **nh)
{
	char *out;
	char *tmp;

	out = ft_strjoin(hashname, " ");
	tmp = out;
	out = isfile ? ft_strjoin(out, "(") : ft_strjoin(out, "(\"");
	ft_strdel(&tmp);
	tmp = out;
	out = ft_strjoin(out, nh[0]);
	ft_strdel(&tmp);
	tmp = out;
	out = isfile ? ft_strjoin(out, ") = ") : ft_strjoin(out, "\") = ");
	ft_strdel(&tmp);
	tmp = out;
	out = ft_strjoin(out, nh[1]);
	ft_strdel(&tmp);
	return (out);
}

char			*md5printfileors(t_sslmodule *module, int isfile,
char *hashname, char **nh)
{
	int		rflag;
	int		qflag;
	char	*out;
	char	*tmp;

	qflag = findflag(module->flags, "-q")->option;
	rflag = findflag(module->flags, "-r")->option;
	if (qflag & 1)
		out = ft_strdup(nh[1]);
	else
	{
		if (rflag & 1)
		{
			out = ft_strjoin(nh[1], " ");
			tmp = out;
			out = ft_strjoin(out, nh[0]);
			ft_strdel(&tmp);
		}
		else
			out = md5genqr(isfile, hashname, nh);
	}
	return (out);
}

void			md5checkandsread(t_sslmodule *module)
{
	t_flag	*sflag;
	char	**i;
	char	*out;

	out = NULL;
	sflag = findflag(module->flags, "-s");
	if (checkflagoption(sflag, 1))
	{
		if (!(i = (char**)malloc(sizeof(char*) * 2)))
			error("Memory is not allocated");
		i[0] = sflag->argv[0];
		i[1] = md5toa(md5(sflag->argv[0], ft_strlen(sflag->argv[0])));
		out = md5printfileors(module, 0, "MD5", i);
		free(i[0]);
		free(i[1]);
		free(i);
		module->output = addoutput(module->output, out);
	}
}

void			md5readfiles(t_sslmodule *module)
{
	char		**i;
	int			err;
	char		*filebody;
	char		*out;
	size_t		count;

	count = -1;
	while (module->filenames[++count] != NULL)
	{
		err = 0;
		filebody = readfile(module->filenames[count], &err);
		if (err)
		{
			md5fileerror(module, err, module->filenames[count]);
			continue ;
		}
		if (!(i = (char**)malloc(sizeof(char*) * 2)))
			error("Memory is not allocated");
		i[0] = module->filenames[count];
		i[1] = md5toa(md5(filebody, ft_strlen(filebody)));
		out = md5printfileors(module, 1, "MD5", i);
		md5freeiandfilebody(i, filebody);
		module->output = addoutput(module->output, out);
	}
}
