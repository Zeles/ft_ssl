/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/10 20:06:45 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 19:44:00 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

t_flag			**md5flags(void)
{
	t_flag	**flag;

	if (!(flag = (t_flag**)malloc(sizeof(t_flag*) * 6)))
		error("Memory is not allocated");
	flag[0] = newflag("-h", 0, "help. ignore all other flag.");
	flag[1] = newflag("-p", 0, "echo STDIN to STDOUT and append the \
	checksum to STDOUT.");
	flag[2] = newflag("-q", 0, "quiet mode.");
	flag[3] = newflag("-r", 0, "reverse the format of the output.");
	flag[4] = newflag("-s", 2, "print the sum of the given string.");
	flag[5] = NULL;
	return (flag);
}

void			md5init(void *module)
{
	((t_sslmodule*)module)->flags = md5flags();
	((t_sslmodule*)module)->name = "md5";
}

void			md5useflag(t_sslmodule *module)
{
	uint	count;

	count = 0;
	while (module->flags[count] != NULL)
	{
		printhelp(module->flags[count], module);
		count++;
	}
}

void			md5core(void *module)
{
	t_sslmodule *mod;

	mod = (t_sslmodule*)module;
	parseflags(mod);
	md5useflag(mod);
	md5readstdin(mod);
	md5checkandsread(mod);
	md5readfiles(mod);
	printoutput(1, mod->output);
}
