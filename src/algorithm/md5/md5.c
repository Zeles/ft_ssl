/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:33:50 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 12:30:34 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

const uint32_t	g_r[] = {
	7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17,
	22, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 4, 11, 16,
	23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 6, 10, 15, 21, 6, 10, 15,
	21, 6, 10, 15, 21, 6, 10, 15, 21
};

const uint32_t	g_k[] = {
	0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
	0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
	0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
	0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
	0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
	0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
	0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
	0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
	0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
	0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
	0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
	0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
	0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
	0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
	0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
	0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391};

char			*paddingmd5(char *msg, int len, uint32_t *bufferlen)
{
	char		*buffer;
	uint32_t	bits_len;

	*bufferlen = len + 1;
	while (*bufferlen % 64 != 56)
		(*bufferlen)++;
	if (!(buffer = (char*)malloc(sizeof(char) * (*bufferlen + 64))))
		error("Memory is not allocated");
	ft_bzero(buffer, *bufferlen + 64);
	ft_memcpy(buffer, msg, len);
	*(uint32_t*)(buffer + len) = 128;
	bits_len = 8 * len;
	ft_memcpy(buffer + *bufferlen, &bits_len, 4);
	return (buffer);
}

void			posswap(uint32_t *tmp, uint32_t *f, uint32_t *g, int c)
{
	if (c < 16)
	{
		*f = F(tmp[1], tmp[2], tmp[3]);
		*g = c;
	}
	else if (c < 32)
	{
		*f = G(tmp[1], tmp[2], tmp[3]);
		*g = (5 * c + 1) % 16;
	}
	else if (c < 48)
	{
		*f = H(tmp[1], tmp[2], tmp[3]);
		*g = (3 * c + 5) % 16;
	}
	else
	{
		*f = I(tmp[1], tmp[2], tmp[3]);
		*g = (7 * c) % 16;
	}
}

void			secondloopmd5(uint32_t *tmp, uint32_t *w, int c)
{
	uint32_t	t;
	uint32_t	f;
	uint32_t	g;

	posswap(tmp, &f, &g, c);
	t = tmp[3];
	tmp[3] = tmp[2];
	tmp[2] = tmp[1];
	tmp[1] = tmp[1] + LROT((tmp[0] + f + g_k[c] + w[g]), g_r[c]);
	tmp[0] = t;
}

void			mainloopmd5(uint32_t *h, char *buffer, uint32_t bufferlen)
{
	uint32_t	*w;
	uint32_t	tmp[4];
	uint32_t	count;
	int			count2;

	count = 0;
	while (count < bufferlen)
	{
		w = (uint32_t*)(buffer + count);
		tmp[0] = h[0];
		tmp[1] = h[1];
		tmp[2] = h[2];
		tmp[3] = h[3];
		count2 = -1;
		while (++count2 < 64)
			secondloopmd5(tmp, w, count2);
		h[0] += tmp[0];
		h[1] += tmp[1];
		h[2] += tmp[2];
		h[3] += tmp[3];
		count += 64;
	}
}

uint32_t		*md5(char *msg, int len)
{
	char		*out;
	uint32_t	*h;
	uint32_t	bufferlen;

	h = md5hinit();
	out = paddingmd5(msg, len, &bufferlen);
	mainloopmd5(h, out, bufferlen);
	free(out);
	return (h);
}
