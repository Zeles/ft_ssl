/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   norm.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 16:17:14 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 18:15:41 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

void			md5fileerror(t_sslmodule *module, int err, char *filename)
{
	char *out;

	if (err & 1)
		out = md5gennosuchfileordir(filename);
	else
		out = md5gendir(filename);
	module->output = addoutput(module->output, out);
}

char			*md5gendir(char *filename)
{
	char	*out;
	char	*tmp;

	out = ft_strjoin("md5: ", filename);
	tmp = out;
	out = ft_strjoin(out, ": Is a directory");
	free(tmp);
	tmp = out;
	out = ft_strjoin("ft_ssl: ", out);
	free(tmp);
	return (out);
}

char			*md5gennosuchfileordir(char *filename)
{
	char	*out;
	char	*tmp;

	out = ft_strjoin("md5: ", filename);
	tmp = out;
	out = ft_strjoin(out, ": No such file or directory");
	free(tmp);
	tmp = out;
	out = ft_strjoin("ft_ssl: ", out);
	free(tmp);
	return (out);
}

void			md5freeiandfilebody(char **i, char *filebody)
{
	free(i[1]);
	free(i);
	ft_strdel(&filebody);
}
