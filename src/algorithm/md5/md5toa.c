/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5toa.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 20:06:10 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 15:41:16 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

char			*md5toa(uint32_t *h)
{
	char		*str;
	char		*tmp;
	char		*tmp2;
	int			i;

	i = -1;
	str = ft_strnew(0);
	while (++i < 4)
	{
		tmp = str;
		str = ft_uitoa_base(reverse32(h[i]), 16);
		tmp2 = str;
		if (ft_strlen(str) < 8)
		{
			str = addzero(str, 8);
			ft_strdel(&tmp2);
			tmp2 = str;
		}
		str = ft_strjoin(tmp, str);
		ft_strdel(&tmp);
		ft_strdel(&tmp2);
	}
	free(h);
	return (str);
}
