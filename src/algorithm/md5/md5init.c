/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 20:21:58 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 20:24:36 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

uint32_t		*md5hinit(void)
{
	uint32_t	*h;

	if (!(h = (uint32_t*)malloc(sizeof(uint32_t) * 4)))
		error("Memory is not allocated");
	h[0] = 0x67452301;
	h[1] = 0xefcdab89;
	h[2] = 0x98badcfe;
	h[3] = 0x10325476;
	return (h);
}
