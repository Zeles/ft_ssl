/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256toa.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 20:07:05 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 15:41:12 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha2.h"

char			*sha256toa(uint32_t *h)
{
	char	*str;
	char	*tmp;
	char	*tmp2;
	size_t	i;

	i = -1;
	str = ft_strnew(0);
	while (++i < 8)
	{
		tmp = str;
		str = ft_uitoa_base(h[i], 16);
		tmp2 = str;
		if (ft_strlen(str) < 8)
		{
			str = addzero(str, 8);
			ft_strdel(&tmp2);
			tmp2 = str;
		}
		str = ft_strjoin(tmp, str);
		ft_strdel(&tmp);
		ft_strdel(&tmp2);
	}
	free(h);
	return (str);
}
