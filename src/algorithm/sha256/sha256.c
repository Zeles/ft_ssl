/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/10 20:06:52 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 20:23:09 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha2.h"

const uint32_t	g_w[64] = {
	0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5,
	0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5,
	0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3,
	0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174,
	0xE49B69C1, 0xEFBE4786, 0x0FC19DC6, 0x240CA1CC,
	0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
	0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7,
	0xC6E00BF3, 0xD5A79147, 0x06CA6351, 0x14292967,
	0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13,
	0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85,
	0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3,
	0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
	0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5,
	0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3,
	0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208,
	0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2
};

void			sha256copyw(uint32_t *ptr, uint32_t *w)
{
	uint32_t i;

	i = -1;
	while (++i < 16)
		w[i] = reverse32(ptr[i]);
	i--;
	while (++i < 64)
		w[i] = SSIG1(w[i - 2]) + w[i - 7] + SSIG0(w[i - 15]) + w[i - 16];
}

void			sha256update(uint32_t *h, uint32_t *tmp)
{
	uint32_t	count;

	count = -1;
	while (++count < 8)
		h[count] += tmp[count];
}

void			sha256block(uint32_t *ptr, uint32_t *h)
{
	uint32_t	i;
	uint32_t	w[64];
	uint32_t	t1;
	uint32_t	t2;
	uint32_t	tmp[8];

	sha256copyw(ptr, w);
	ft_memcpy(tmp, h, 8 * sizeof(uint32_t));
	i = 0;
	while (i < 64)
	{
		t1 = tmp[7] + BSIG1(tmp[4]) + CH(tmp[4], tmp[5], tmp[6])
		+ g_w[i] + w[i];
		t2 = BSIG0(tmp[0]) + MAJ(tmp[0], tmp[1], tmp[2]);
		tmp[7] = tmp[6];
		tmp[6] = tmp[5];
		tmp[5] = tmp[4];
		tmp[4] = tmp[3] + t1;
		tmp[3] = tmp[2];
		tmp[2] = tmp[1];
		tmp[1] = tmp[0];
		tmp[0] = t1 + t2;
		i++;
	}
	sha256update(h, tmp);
}

char			*sha256padding(char *msg, uint32_t len, uint32_t *bufferlen)
{
	uint32_t		newlen;
	char			*newmsg;

	newlen = len + 1;
	newlen += (64 - newlen % 64);
	if (!(newmsg = (char*)malloc(sizeof(char) * newlen)))
		error("Memory is not allocated");
	bzero(newmsg, newlen);
	ft_memcpy(newmsg, msg, len);
	newmsg[len] = 0x80;
	(newmsg)[newlen - 4] = ((len * 8) >> 24) & 0xFF;
	(newmsg)[newlen - 3] = ((len * 8) >> 16) & 0xFF;
	(newmsg)[newlen - 2] = ((len * 8) >> 8) & 0xFF;
	(newmsg)[newlen - 1] = (len * 8) & 0xFF;
	*bufferlen = newlen;
	return (newmsg);
}

uint32_t		*sha256(char *msg, size_t len)
{
	char		*out;
	char		*tmp;
	uint32_t	bufferlen;
	uint32_t	*h;

	h = sha256inithbuff();
	out = sha256padding(msg, (uint32_t)len, &bufferlen);
	tmp = out;
	while (bufferlen)
	{
		sha256block((uint32_t*)out, h);
		out += 64;
		bufferlen -= 64;
	}
	ft_memdel((void**)&tmp);
	return (h);
}
