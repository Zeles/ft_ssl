/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256init.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 20:22:51 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 20:24:34 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha2.h"

uint32_t		*sha256inithbuff(void)
{
	uint32_t	*h;

	if (!(h = (uint32_t*)malloc(sizeof(uint32_t) * 8)))
		error("Memory is not allocated");
	h[0] = 0x6A09E667;
	h[1] = 0xBB67AE85;
	h[2] = 0x3C6EF372;
	h[3] = 0xA54FF53A;
	h[4] = 0x510E527F;
	h[5] = 0x9B05688C;
	h[6] = 0x1F83D9AB;
	h[7] = 0x5BE0CD19;
	return (h);
}
