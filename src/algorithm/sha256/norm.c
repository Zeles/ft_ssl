/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   norm.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 16:16:40 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 18:16:20 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha2.h"

void			sha256fileerror(t_sslmodule *module, int err, char *filename)
{
	char *out;

	if (err & 1)
		out = sha256gennosuchfileordir(filename);
	else
		out = sha256gendir(filename);
	module->output = addoutput(module->output, out);
}

char			*sha256gendir(char *filename)
{
	char	*out;
	char	*tmp;

	out = ft_strjoin("sha256: ", filename);
	tmp = out;
	out = ft_strjoin(out, ": Is a directory");
	free(tmp);
	tmp = out;
	out = ft_strjoin("ft_ssl: ", out);
	free(tmp);
	return (out);
}

char			*sha256gennosuchfileordir(char *filename)
{
	char	*out;
	char	*tmp;

	out = ft_strjoin("sha256: ", filename);
	tmp = out;
	out = ft_strjoin(out, ": No such file or directory");
	free(tmp);
	tmp = out;
	out = ft_strjoin("ft_ssl: ", out);
	free(tmp);
	return (out);
}

void			sha256freeiandfilebody(char **i, char *filebody)
{
	free(i[1]);
	free(i);
	ft_strdel(&filebody);
}
