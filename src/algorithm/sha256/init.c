/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/10 20:06:56 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 19:44:53 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha2.h"

t_flag			**sha256flags(void)
{
	t_flag	**flag;

	if (!(flag = (t_flag**)malloc(sizeof(t_flag*) * 6)))
		error("Memory is not allocated");
	flag[0] = newflag("-h", 0, "help. ignore all other flag.");
	flag[1] = newflag("-p", 0, "echo STDIN to STDOUT and append \
	the checksum to STDOUT");
	flag[2] = newflag("-q", 0, "quiet mode");
	flag[3] = newflag("-r", 0, "reverse the format of the output");
	flag[4] = newflag("-s", 2, "print the sum of the given string");
	flag[5] = NULL;
	return (flag);
}

void			sha256init(void *module)
{
	((t_sslmodule*)module)->flags = sha256flags();
	((t_sslmodule*)module)->name = "sha256";
}

void			sha256useflag(t_sslmodule *module)
{
	uint	count;

	count = 0;
	while (module->flags[count] != NULL)
	{
		printhelp(module->flags[count], module);
		count++;
	}
}

void			sha256core(void *module)
{
	t_sslmodule	*mod;

	mod = (t_sslmodule*)module;
	parseflags(mod);
	sha256useflag(mod);
	sha256readstdin(mod);
	sha256checkandsread(mod);
	sha256readfiles(mod);
	printoutput(1, mod->output);
}
