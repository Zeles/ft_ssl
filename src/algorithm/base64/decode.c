/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   decode.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:32:35 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 18:36:38 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "base64.h"

const int g_b64invs[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

size_t		decodebase64len(char *msg)
{
	size_t	len;
	size_t	rlen;
	size_t	i;

	len = ft_strlen(msg);
	rlen = len / 4 * 3;
	i = len;
	while (i > 0)
	{
		if (msg[i] == '=')
			rlen--;
		else
			break ;
		i--;
	}
	return (rlen);
}

int			isvalidcharbase64(char c)
{
	if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') ||
	(c >= 'a' && c <= 'z') || c == '+' || c == '/' || c == '=')
		return (1);
	return (0);
}

int			msgvalidbase64(char *msg, size_t len)
{
	size_t	count;

	count = 0;
	while (count < len)
	{
		if (!isvalidcharbase64(msg[count]))
			return (0);
		count++;
	}
	return (1);
}

void		decodeloopbase64(size_t *i, size_t *j, char *msg,
unsigned char *out)
{
	int		v;

	v = g_b64invs[msg[*i] - 43];
	v = (v << 6) | g_b64invs[msg[*i + 1] - 43];
	v = msg[*i + 2] == '=' ? v << 6 : (v << 6) |
	g_b64invs[msg[*i + 2] - 43];
	v = msg[*i + 3] == '=' ? v << 6 : (v << 6) |
	g_b64invs[msg[*i + 3] - 43];
	out[*j] = (v >> 16) & 0xFF;
	if (msg[*i + 2] != '=')
		out[*j + 1] = (v >> 8) & 0xFF;
	if (msg[*i + 3] != '=')
		out[*j + 2] = v & 0xFF;
	*i += 4;
	*j += 3;
}

char		*decodebase64(char *msg, unsigned char *out, size_t outlen)
{
	size_t			len;
	size_t			ij[2];

	len = ft_strlen(msg);
	if (outlen < decodebase64len(msg) || len % 4 != 0)
		error("BASE64: invalid message length");
	if (!msgvalidbase64(msg, len))
		error("BASE64: invalid characters in the message");
	ij[0] = 0;
	ij[1] = 0;
	while (ij[0] < len)
		decodeloopbase64(&ij[0], &ij[1], msg, out);
	return ((char*)out);
}
