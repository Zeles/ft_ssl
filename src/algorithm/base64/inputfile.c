/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inputfile.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:30:41 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 13:17:37 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "base64.h"

void			base64readfile(t_sslmodule *module, char *filename, int mod)
{
	int			err;
	size_t		newlen;
	char		*msg;
	char		*filebody;
	char		*out;

	err = 0;
	filebody = readfile(filename, &err);
	if ((int)ft_strlen(filebody) - 1 < 0)
		error("BASE64: input is empty");
	msg = ft_strsub(filebody, 0, ft_strlen(filebody) - 1);
	if (mod)
		out = encodebase64(msg, ft_strlen(msg));
	else
	{
		newlen = decodebase64len(msg) + 1;
		if (!(out = ft_strnew(newlen)))
			error("Memory is not allocated");
		out = decodebase64(msg, (unsigned char*)out, newlen);
	}
	module->output = addoutput(module->output, out);
}
