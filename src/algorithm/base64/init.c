/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:30:46 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 19:35:01 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "base64.h"

t_flag			**base64flags(void)
{
	t_flag	**flag;

	if (!(flag = (t_flag**)malloc(sizeof(t_flag*) * 6)))
		error("Memory is not allocated");
	flag[0] = newflag("-h", 0, "help. ignore all other flag.");
	flag[1] = newflag("-d", 0, "decode mode");
	flag[2] = newflag("-e", 0, "encode mode (default)");
	flag[3] = newflag("-i", 2, "input file");
	flag[4] = newflag("-o", 2, "output file");
	flag[5] = NULL;
	return (flag);
}

void			base64init(void *module)
{
	((t_sslmodule*)module)->flags = base64flags();
	((t_sslmodule*)module)->name = "base64";
}

void			base64useflag(t_sslmodule *module)
{
	uint	count;

	count = 0;
	while (module->flags[count] != NULL)
	{
		printhelp(module->flags[count], module);
		count++;
	}
}

void			base64readstdin(t_sslmodule *module, int mod)
{
	char		*filebody;
	char		*msg;
	size_t		newlen;
	char		*out;

	filebody = readfd(0, NULL);
	if ((int)ft_strlen(filebody) - 1 < 0)
		error("SHA256: input is empty");
	msg = ft_strsub(filebody, 0, ft_strlen(filebody) - 1);
	free(filebody);
	if (mod)
		out = encodebase64(msg, ft_strlen(msg));
	else
	{
		newlen = decodebase64len(msg) + 1;
		if (!(out = ft_strnew(newlen)))
			error("Memory is not allocated");
		out = decodebase64(msg, (unsigned char*)out, newlen);
	}
	module->output = addoutput(module->output, out);
}

void			base64core(void *module)
{
	t_sslmodule	*mod;
	t_flag		*iflag;
	t_flag		*oflag;
	int			dflag;

	mod = ((t_sslmodule*)module);
	parseflags(mod);
	base64useflag(module);
	iflag = findflag(mod->flags, "-i");
	oflag = findflag(mod->flags, "-o");
	dflag = !checkflagoption(findflag(mod->flags, "-d"), 1);
	if (checkflagoption(iflag, 1))
		base64readfile(module, iflag->argv[0], dflag);
	else
		base64readstdin(module, dflag);
	if (checkflagoption(oflag, 1))
		base64printfile(oflag->argv[0], mod->output);
	else
		printoutput(1, mod->output);
}
