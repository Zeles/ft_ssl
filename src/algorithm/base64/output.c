/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   output.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:30:44 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 20:15:02 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "base64.h"

void			base64printfile(char *filename, char **output)
{
	int		fd;

	fd = open(filename, O_CREAT | O_RDWR, S_IWRITE | S_IREAD);
	if (fd <= 0)
		logerror(ft_strjoin(ft_strjoin("base64: ", filename),
		": No such file or directory"));
	printoutput(fd, output);
	close(fd);
}
