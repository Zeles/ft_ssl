/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   encode.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:30:48 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 18:04:48 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "base64.h"

const char	g_b64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabc\
defghijklmnopqrstuvwxyz0123456789+/";

size_t		encodebase64len(size_t len)
{
	size_t	rlen;

	rlen = len;
	if (rlen % 3 != 0)
		rlen += 3 - (len % 3);
	rlen /= 3;
	rlen *= 4;
	return (rlen);
}

void		encodeloopbase64(size_t *i, size_t *j, char **msgs, size_t len)
{
	size_t	v;

	v = msgs[0][*i];
	v = *i + 1 < len ? v << 8 | msgs[0][*i + 1] : v << 8;
	v = *i + 2 < len ? v << 8 | msgs[0][*i + 2] : v << 8;
	msgs[1][*j] = g_b64chars[(v >> 18) & 63];
	msgs[1][*j + 1] = g_b64chars[(v >> 12) & 63];
	if (*i + 1 < len)
		msgs[1][*j + 2] = g_b64chars[(v >> 6) & 63];
	else
		msgs[1][*j + 2] = '=';
	if (*i + 2 < len)
		msgs[1][*j + 3] = g_b64chars[v & 63];
	else
		msgs[1][*j + 3] = '=';
	*i += 3;
	*j += 4;
}

char		*encodebase64(char *msg, size_t len)
{
	char	*msgs[2];
	char	*out;
	size_t	newlen;
	size_t	ij[2];

	if (len == 0)
		return (NULL);
	newlen = encodebase64len(len);
	if (!(out = (char*)malloc(sizeof(char) * (newlen + 1))))
		error("Memory is not allocated");
	out[newlen] = '\0';
	ij[0] = 0;
	ij[1] = 0;
	msgs[0] = msg;
	msgs[1] = out;
	while (ij[0] < len)
		encodeloopbase64(&ij[0], &ij[1], msgs, len);
	return (out);
}
