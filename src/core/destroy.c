/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   destroy.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 12:50:43 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 15:42:08 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	coredestroy(t_sslcore *core)
{
	size_t	count;

	count = 0;
	while (core->modules[count] != NULL)
	{
		moduledestroy(core->modules[count]);
		free(core->modules[count]);
		count++;
	}
	free(core->modules);
}
