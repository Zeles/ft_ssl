/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/10 20:06:59 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 18:30:36 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void			loadmodule(t_sslcore *core, int argc, char **argv)
{
	t_argv ar;

	if (!(core->modules = (t_sslmodule**)malloc(sizeof(t_sslmodule*) * 4)))
		error("Memory is not allocated");
	ar = initargv(argc - 2, &argv[2]);
	core->modules[0] = newsslmodule(HASH, ar, md5init, md5core);
	core->modules[1] = newsslmodule(HASH, ar, sha256init, sha256core);
	core->modules[2] = newsslmodule(CIPHER, ar, base64init, base64core);
	core->modules[3] = NULL;
}

t_sslcore		*newsslcore(int argc, char **argv)
{
	t_sslcore	*newcore;

	if (!(newcore = (t_sslcore*)malloc(sizeof(t_sslcore))))
		error("Memory is not allocated");
	loadmodule(newcore, argc, argv);
	return (newcore);
}
