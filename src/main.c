/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/10 20:06:49 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 18:39:53 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	errorinvalidcommand(char *argv1, t_sslmodule **modules)
{
	char	*err;
	char	*tmp;

	err = ft_strjoin("Error: '", argv1);
	tmp = err;
	err = ft_strjoin(tmp, "'");
	ft_strdel(&tmp);
	tmp = err;
	err = ft_strjoin(tmp, " is an invalid command.");
	ft_strdel(&tmp);
	logerror(err);
	printtype(modules);
	exit(EXIT_FAILURE);
}

void	parsehashtype(t_sslcore *core, char **argv)
{
	uint	flag;
	uint	count;

	count = 0;
	flag = 0;
	while (core->modules[count] != NULL)
	{
		if (!ft_strcmp(argv[1], core->modules[count]->name))
		{
			(core->modules[count]->hash)(core->modules[count]);
			flag = 1;
		}
		count++;
	}
	if (!flag)
		errorinvalidcommand(argv[1], core->modules);
}

int		main(int argc, char **argv)
{
	t_sslcore	*core;

	if (argc < 2)
		baseusage(argv[0]);
	core = newsslcore(argc, argv);
	parsehashtype(core, argv);
	coredestroy(core);
	free(core);
	return (0);
}
