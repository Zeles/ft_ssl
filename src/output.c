/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   output.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/10 20:09:48 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 15:34:17 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

char				**addoutput(char **output, char *newelement)
{
	char	**newoutput;
	size_t	count;

	count = 0;
	while (output[count] != NULL)
		count++;
	if (!(newoutput = (char**)malloc(sizeof(char*) * (count + 2))))
		error("Memory is not allocated");
	count = 0;
	while (output[count] != NULL)
	{
		newoutput[count] = output[count];
		count++;
	}
	newoutput[count] = newelement;
	newoutput[count + 1] = NULL;
	free(output);
	return (newoutput);
}

void				printoutput(int fd, char **output)
{
	size_t	count;

	count = 0;
	while (output[count] != NULL)
	{
		ft_putendl_fd(output[count], fd);
		ft_strdel(&(output[count]));
		count++;
	}
}
