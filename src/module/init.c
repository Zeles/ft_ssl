/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:19:42 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 19:45:37 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

t_sslmodule			*newsslmodule(int type, t_argv argv,
void (*init)(void*), void (*hash)(void*))
{
	t_sslmodule	*newmodule;

	if (!(newmodule = (t_sslmodule*)malloc(sizeof(t_sslmodule))))
		error("Memory is not allocated");
	newmodule->name = NULL;
	newmodule->type = type;
	newmodule->flags = NULL;
	newmodule->argv = argv;
	newmodule->hash = hash;
	newmodule->init = init;
	newmodule->flagcount = 0;
	newmodule->filecount = 0;
	(newmodule->init)(newmodule);
	if (!(newmodule->output = (char**)malloc(sizeof(char*))))
		error("Memory is not allocated");
	newmodule->output[0] = NULL;
	if (!(newmodule->filenames = (char**)malloc(sizeof(char*))))
		error("Memory is not allocated");
	newmodule->filenames[0] = NULL;
	return (newmodule);
}
