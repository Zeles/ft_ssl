/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   addoutput.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:29:18 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 17:29:30 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

char		*addoutput(char **output, char *newelement)
{
	char	**newoutput;
	size_t	count;

	count = 0;
	while (output[count] != NULL)
		count++;
	if (!(newoutput = (char**)malloc(sizeof(char*) * count)))
		error("Memory is not allocated");
	count = 0;
	while (output[count] != NULL)
	{
		newoutput[count] = output[count];
		count++;
	}
	newoutput[count] = newelement;
	newoutput[count + 1] = NULL;
	ft_memdel((void**)output);
	return (newoutput);
}
