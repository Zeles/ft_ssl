/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   destroy.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 12:52:01 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 15:42:11 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void		moduledestroy(t_sslmodule *module)
{
	size_t	count;

	count = 0;
	while (module->flags[count] != NULL)
	{
		flagdestroy(module->flags[count]);
		count++;
	}
	free(module->output);
	free(module->filenames);
	free(module->flags);
}
