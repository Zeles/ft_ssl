/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   destroy.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 12:53:46 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 15:42:14 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void		flagdestroy(t_flag *flag)
{
	size_t	count;

	count = 0;
	while (count < flag->argc)
	{
		ft_strdel(&flag->argv[count]);
		count++;
	}
	free(flag->argv);
	ft_memdel((void**)&flag);
}
