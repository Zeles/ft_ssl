/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:20:03 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 13:05:42 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

t_flag			*newflag(char *flag, int option, const char *description)
{
	t_flag	*result;

	if (!(result = (t_flag*)malloc(sizeof(t_flag))))
		error("Memory is not allocated");
	result->argc = 0;
	result->argv = NULL;
	result->flag = flag;
	result->option = option;
	result->description = description;
	return (result);
}

char			**addfilenames(char **filenames, char *newelement)
{
	char	**newfilenames;
	size_t	count;

	count = 0;
	while (filenames[count] != NULL)
		count++;
	if (!(newfilenames = (char**)malloc(sizeof(char*) * (count + 2))))
		error("Memory is not allocated");
	count = 0;
	while (filenames[count] != NULL)
	{
		newfilenames[count] = filenames[count];
		count++;
	}
	newfilenames[count] = newelement;
	newfilenames[count + 1] = NULL;
	free(filenames);
	return (newfilenames);
}

void			appendargvtoflag(t_flag *flag, char *argv)
{
	char	**newargv;
	uint	count;

	count = 0;
	if (flag->argv != NULL)
		while (flag->argv[count] != NULL)
			count++;
	if (!(newargv = (char**)malloc(sizeof(char*) * (count + 2))))
		error("Memory is not allocated");
	count = 0;
	if (flag->argv != NULL)
		while (flag->argv[count] != NULL)
		{
			newargv[count] = flag->argv[count];
			count++;
		}
	newargv[count] = ft_strdup(argv);
	newargv[count + 1] = NULL;
	ft_strdel(flag->argv);
	flag->argv = newargv;
}
