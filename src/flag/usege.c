/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usege.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:19:27 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 21:49:16 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	baseusage(char *argv0)
{
	ft_putstr("Usage: ");
	ft_putstr(argv0);
	ft_putendl(" command [command opts] [command args]");
	exit(EXIT_FAILURE);
}

void	flagusage(t_sslmodule *module)
{
	uint	count;

	ft_putstr("Usage flag: ");
	ft_putstr("ft_ssl");
	ft_putchar(' ');
	ft_putstr(module->name);
	ft_putendl(" [command opts] [command args]");
	count = 0;
	while (module->flags[count] != NULL)
	{
		ft_putchar('\t');
		ft_putstr(module->flags[count]->flag);
		ft_putchar('\t');
		ft_putendl(module->flags[count]->description);
		count++;
	}
	exit(EXIT_FAILURE);
}
