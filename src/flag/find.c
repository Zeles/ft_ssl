/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/10 20:44:02 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/11 17:29:42 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

t_flag		*findflag(t_flag **flags, char *flag)
{
	size_t	count;

	count = 0;
	while (flags[count] != NULL)
	{
		if (!ft_strcmp(flags[count]->flag, flag))
			return (flags[count]);
		count++;
	}
	return (NULL);
}
