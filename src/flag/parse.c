/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:20:47 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 00:15:14 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

int				isthisflag(t_flag *flag, char *argv)
{
	if (!ft_strncmp(argv, flag->flag, ft_strlen(argv)))
		return (1);
	return (0);
}

int				isflag(t_flag **flags, char *argv)
{
	int	count;

	count = 0;
	while (flags[count] != NULL)
	{
		if (isthisflag(flags[count], argv))
			return (count);
		count++;
	}
	return (-1);
}

int				checkflagoption(t_flag *flag, int option)
{
	if (flag->option & option)
		return (1);
	return (0);
}

void			addflag(t_flag *flag, int *count, t_argv ar)
{
	if (!checkflagoption(flag, 1))
	{
		if (checkflagoption(flag, 2))
		{
			if (*count + 1 >= ar.argc)
				error(ft_strjoin(flag->flag, " without second command"));
			appendargvtoflag(flag, ar.argv[*count + 1]);
			(*count)++;
		}
		flag->option |= 1;
	}
	else
		error(ft_strjoin(flag->flag, " already use"));
}

void			parseflags(t_sslmodule *module)
{
	int		id;

	while (module->flagcount < module->argv.argc)
	{
		if ((id = isflag(module->flags,
		module->argv.argv[module->flagcount])) != -1)
			addflag(module->flags[id], &module->flagcount, module->argv);
		else
			break ;
		module->flagcount++;
	}
	while (module->flagcount < module->argv.argc)
	{
		module->filenames = addfilenames(module->filenames,
		module->argv.argv[module->flagcount]);
		module->flagcount++;
		module->filecount++;
	}
}
