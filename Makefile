NAME = ft_ssl

SRC = main.c output.c \
		algorithm/md5/init.c algorithm/md5/norm.c algorithm/md5/md5.c algorithm/md5/md5init.c algorithm/md5/read.c algorithm/md5/md5toa.c \
		algorithm/sha256/init.c algorithm/sha256/norm.c algorithm/sha256/sha256.c algorithm/sha256/sha256init.c algorithm/sha256/read.c algorithm/sha256/sha256toa.c \
		algorithm/base64/init.c algorithm/base64/decode.c algorithm/base64/encode.c algorithm/base64/inputfile.c algorithm/base64/output.c \
		argv/init.c \
		core/init.c core/destroy.c \
		flag/init.c flag/destroy.c flag/parse.c flag/usege.c flag/find.c \
		module/init.c module/destroy.c \
		utils/error.c utils/help.c utils/readfile.c utils/genhexarr.c utils/reverse.c utils/addzero.c

DIRS = algorithm algorithm/md5 algorithm/sha256 algorithm/base64 \
		argv core flag module utils \

OBJ_NAME = $(SRC:.c=.o)
OBJ_DIR = ./obj/
OBJ = $(addprefix $(OBJ_DIR),$(OBJ_NAME))

SRC_DIR = ./src/
INCLUDES_DIR = ./include/
LIB_DIR = ./lib/

LIBFT_DIR = $(LIB_DIR)libft/
LIBFT_INC = -I $(LIBFT_DIR)
LIBFT_LNK = $(LIBFT_INC) -L $(LIBFT_DIR) -lft

FLAG = -g -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJ)
	$(MAKE) -j -C $(LIBFT_DIR)
	gcc $(FLAG) $(OBJ) $(LIBFT_LNK) -o $(NAME)

clean:
	$(MAKE) -j -C $(LIBFT_DIR) clean
	rm -rf $(OBJ)
	rm -rf $(OBJ_DIR)

fclean: clean
	$(MAKE) -j -C $(LIBFT_DIR) fclean
	rm -rf $(NAME)

$(OBJ_DIR)%.o:$(SRC_DIR)%.c
	@mkdir -p $(@D)
	gcc $(FLAG) $(LIBFT_INC) -I $(INCLUDES_DIR)  -o $@ -c $<

re:	fclean $(NAME)
