/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/12 18:55:34 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 19:32:44 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_H
# define FT_SSL_H
# include "../lib/libft/includes/libft.h"
# include "md5.h"
# include "sha2.h"
# include "base64.h"

typedef enum		e_type
{
	STANDART,
	HASH,
	CIPHER
}					t_type;

typedef struct		s_argv
{
	int			argc;
	char		**argv;
}					t_argv;

typedef struct		s_flag
{
	char			*flag;
	int				option;
	char			**argv;
	unsigned int	argc;
	const char		*description;
}					t_flag;

typedef struct		s_sslmodule
{
	enum e_type		type;
	char			*name;
	t_flag			**flags;
	char			*stdin;
	char			**output;
	void			(*init)(void*);
	void			(*hash)(void*);
	char			**filenames;
	int				flagcount;
	int				filecount;
	t_argv			argv;
}					t_sslmodule;

typedef struct		s_sslcore
{
	t_sslmodule		**modules;
}					t_sslcore;

t_flag				*newflag(char *flag, int option, const char *description);
void				flagdestroy(t_flag *flag);
void				parseflags(t_sslmodule *module);
char				**addfilenames(char **filenames, char *newelement);
void				appendargvtoflag(t_flag *flag, char *argv);
t_flag				*findflag(t_flag **flags, char *flag);
t_argv				initargv(int argc, char **argv);
t_sslmodule			*newsslmodule(int type, t_argv argv,
void (*init)(void*), void (*hash)(void*));
void				moduledestroy(t_sslmodule *module);
int					checkflagoption(t_flag *flag, int option);
void				flagusage(t_sslmodule *module);
void				printhelp(t_flag *flag, t_sslmodule *module);
char				**addoutput(char **output, char *newelement);
void				printoutput(int fd, char **output);
t_sslcore			*newsslcore(int argc, char **argv);
void				coredestroy(t_sslcore *core);
void				error(char *str);
void				logerror(char *str);
void				baseusage(char *argv0);
void				printtype(t_sslmodule **module);
char				*readfd(int fd, int *err);
char				*readfile(const char *path, int *err);
char				*genhexarr(void);
uint32_t			reverse32(uint32_t t);
char				*addzero(char *str, size_t zerocount);

#endif
