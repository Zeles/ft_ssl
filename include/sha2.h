/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha2.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:25:39 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 19:38:34 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHA2_H
# define SHA2_H
# include "ft_ssl.h"
# define CH(X, Y, Z) (((X) & (Y)) ^ ((~X) & Z))
# define MAJ(X, Y, Z) ((X & Y) ^ (X & Z) ^ (Y & Z))
# define RROT(X, N) (((X) >> (N)) | ((X) << (32 - (N))))
# define SHR(X, N) (X >> N)
# define BSIG0(X) (RROT(X, 2) ^ RROT(X, 13) ^ RROT(X, 22))
# define BSIG1(X) (RROT(X, 6) ^ RROT(X, 11) ^ RROT(X, 25))
# define SSIG0(X) (RROT(X, 7) ^ RROT(X, 18) ^ SHR(X, 3))
# define SSIG1(X) (RROT(X, 17) ^ RROT(X, 19) ^ SHR(X, 10))

typedef struct s_sslmodule	t_sslmodule;

uint32_t		*sha256(char *msg, size_t len);
uint32_t		*sha256inithbuff(void);
void			sha256init(void *module);
void			sha256core(void *module);
char			*sha256toa(uint32_t *h);
void			sha256readfiles(t_sslmodule *module);
void			sha256checkandsread(t_sslmodule *module);
char			*sha256printfileors(t_sslmodule *module, int isfile,
char *hashname, char **nh);
char			*sha256genqr(int isfile, char *hashname, char **nh);
void			sha256readstdin(t_sslmodule *module);

char			*sha256padding(char *msg, uint32_t len, uint32_t *bufferlen);
void			sha256block(uint32_t *ptr, uint32_t *h);
char			*sha256gennosuchfileordir(char *filename);
void			sha256freeiandfilebody(char **i, char *filebody);
char			*sha256gendir(char *filename);
void			sha256fileerror(t_sslmodule *module, int err, char *filename);

#endif
