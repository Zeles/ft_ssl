/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:26:45 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 19:38:38 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MD5_H
# define MD5_H
# include "ft_ssl.h"
# define LROT(x, c) (((x) << (c)) | ((x) >> (32 - (c))))
# define F(X, Y, Z) ((X & Y) | ((~X) & Z))
# define G(X, Y, Z) ((X & Z) | (Y & (~Z)))
# define H(X, Y, Z) (X ^ Y ^ Z)
# define I(X, Y, Z) (Y ^ (X | (~Z)))

typedef struct s_sslmodule	t_sslmodule;

uint32_t		*md5(char *msg, int len);
uint32_t		*md5hinit(void);
void			md5init(void *module);
void			md5core(void *module);
void			md5readfiles(t_sslmodule *module);
void			md5checkandsread(t_sslmodule *module);
char			*md5printfileors(t_sslmodule *module, int isfile,
char *hashname, char **nh);
char			*md5genqr(int isfile, char *hashname, char **nh);
void			md5readstdin(t_sslmodule *module);
char			*md5toa(uint32_t *h);
char			*md5gennosuchfileordir(char *filename);
void			md5freeiandfilebody(char **i, char *filebody);
char			*md5gendir(char *filename);
void			md5fileerror(t_sslmodule *module, int err, char *filename);

#endif
