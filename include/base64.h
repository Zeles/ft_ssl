/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/11 17:26:24 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 19:33:14 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BASE64_H
# define BASE64_H
# include "ft_ssl.h"

typedef struct s_sslmodule	t_sslmodule;

size_t			decodebase64len(char *msg);
char			*decodebase64(char *msg, unsigned char *out, size_t outlen);
char			*encodebase64(char *msg, size_t len);
void			base64readfile(t_sslmodule *module, char *filename, int mod);
void			base64printfile(char *filename, char **output);

void			base64init(void *module);
void			base64core(void *module);

#endif
