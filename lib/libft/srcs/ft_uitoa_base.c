/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_uitoa_base.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 12:18:34 by gdaniel           #+#    #+#             */
/*   Updated: 2020/05/12 15:20:28 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_uitoa_base_len(uint32_t value, int base)
{
	size_t	len;

	len = 0;
	while (value != 0)
	{
		value /= base;
		len++;
	}
	return (len);
}

char	*free_str(char *str)
{
	char *tmp;

	tmp = str;
	str = ft_strsub(str, 1, ft_strlen(str) - 1);
	ft_strdel(&tmp);
	return (str);
}

char	*ft_uitoa_base(uint32_t value, int base)
{
	char	*result;
	char	*regex;
	size_t	len;

	len = ft_uitoa_base_len(value, base);
	regex = "0123456789abcdef";
	if (value == 0)
		return (ft_strdup("0"));
	if (!(result = (char*)malloc(sizeof(char) * (len + 2))))
		return (NULL);
	result[len + 1] = '\0';
	while (len + 1)
	{
		result[len] = regex[value % base];
		value /= base;
		len--;
	}
	if (result[0] == '0')
		result = free_str(result);
	return (result);
}
